# Sharkey Dev Server

This is a prebuilt Sharkey server (version 2024.3.1), designed to run in a virtual desktop instance, for development purposes only. It is **not** intended to run in a live environment or federate with the Fediverse. It may be useful for testing some code changes, or styling options before suggesting changes to the main [Sharkey git](https://activitypub.software/TransFem-org/Sharkey)

You make use of this VM in the spirit in which it is intended, as a test development server. I make NO warranties or guarantees as to the functionality. You use this as your own risk.
 
 Enjoy!

-----

## Prerequisites 

* 64-bit host operating system 
* Oracle's [VM Virtual Box](https://www.virtualbox.org) software -- Free! Also available as a flatpak in Linux.
* The downloaded [SharkeyDevServer.ova](https://ozol.cloud/f/691c464110324055bf7e/) Open Virtual Appliance file - 3.4GB!

This has been tested on the following desktops:

* Ubuntu 22.04 
* KDE Neon 6
* Windows 11

With Virtual Box version 6.1 and 7.0.14

(Please advise if you have tested elsewhere)
## Installation

* Install the VM Virtual Box software on your host machine (Desktop).
* Run Virtual Box
* Click File, Import Appliance and locate the OVA files you downloaded. This will import and build a VM.

You will have the opportunity to select how much memory, and location of the VM being built. Check the Network settings for the VM to select your network card/device.

Once Imported, the OVA file is no longer required. You can delete it, or keep a copy for later use if you wish a fresh start.

## Running

* Start the VM. Ubuntu Linux should start running in a window. Eventual a login prompt will appear. [Screenshot](Screenshot-UbuntuStartSreen.png)
* On the login screen note the IP address shown. If there is no IP address, or a default of 10.x.x.x check your network settings in the VM settings. This build requires a DHCP server on your broadband router/network. Alternatively you can set your own address manually in the settings of the configured VM.
* Login as *babyshark* The password is *letmein*



```
sudo -u sharkey -i
cd Sharkey
pnpm start
```
After a short time and some coloured messages, Sharkey will be running. [Screenshot](Screenshot-SharkeyRunning.png)


You should be able to open your web browser, and enter the IP address noted earlier, going to port 3000

Example: `http://192.168.1.158:3000`

The Sharkey login screen should appear. The Sharley user is *admin* and *letmein* 

A [Screenshot](Screenshot-SharkeyLogin.png) in the browser of login page. Another [Screenshot](Screenshot-SharkeyInBrowser.png) in the browser, looking at the Instance Info screen.

## Shutting Down

On the VM, Press CTRL-C to stop the Sharkey server

On the command line:
```
exit
sudo shutdown -h now
```
(sudo password is letmein)

### Other passwords in the VM:

**PostgreSQL**

```
Database: sharkey
User: sharkey
Password: letmein
```

## Need to Get in touch?

Find me on the Fediverse. Send me a DM:  @daj@iBe.social

If you found this useful, or have ideas, let me know.

